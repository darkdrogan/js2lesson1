/* eslint-disable no-plusplus,no-undef,no-use-before-define */
class Container {
  constructor() {
    this.id = '';
    this.className = '';
    this.htmlCode = '';
    this.element = '';
  }

  render() {
    return this.element;
  }

  remove() {
    this.element.parentNode.removeChild(this.element);
  }
}

class Menu extends Container {
  constructor(myId, myClass, myItems) {
    super();
    this.id = myId;
    this.className = myClass;
    this.items = myItems;
  }

  render() {
    this.element = document.createElement('ul');

    for (let i = 0; i < this.items.length; i++) {
      const el = this.items[i].render();
      this.element.appendChild(el);
    }
    return this.element;
  }
}

class MenuItem extends Menu {
  constructor(myHref, myLabel) {
    super();
    this.className = 'menu-item';
    this.href = myHref;
    this.label = myLabel;
  }

  render() {
    const el = document.createElement('li');
    el.className = this.className;
    el.innerHTML = `<a href="${this.href}">${this.label}</a>`;
    this.element = el;
    return this.element;
  }
}

class SubMenu extends Menu {
  constructor(myClass, myLabel, myItems) {
    super();
    this.className = 'sub-menu';
    this.label = myLabel;
    this.items = myItems;
  }

  render() {
    const ele = document.createElement('li');
    ele.className = this.className;
    ele.innerHTML = `<a href="${this.href}">${this.label}</a>`;

    const elements = document.createElement('ul');

    for (let i = 0; i < this.items.length; i++) {
      if (this.items[i] instanceof MenuItem) {
        const el = this.items[i].render();
        elements.appendChild(el);
      }
    }
    ele.appendChild(elements);
    this.element = ele;
    return this.element;
  }
}
