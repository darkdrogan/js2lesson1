/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
export default function HamburgerException(property) {
  Error.call(this, property);
  this.name = 'HamburgerException';

  this.property = property;
  this.message = `Ошибка в свойстве ${property}`;

  if (Error.captureStackTrace) {
    Error.captureStackTrace(this, HamburgerException);
  } else {
    this.stack = (new Error()).stack;
  }
  console.log(`${this.name}: ${this.message} in property ${this.property}`);
}
