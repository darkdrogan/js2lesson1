/* eslint-disable no-undef,no-use-before-define,no-plusplus */
function Container() {
  this.id = '';
  this.className = '';
  this.htmlCode = '';
  this.element = '';
}

Container.prototype.render = function () {
  return this.element;
};

Container.prototype.remove = function () {
  this.element.parentNode.removeChild(this.element);
};


function Menu(myId, myClass, myItems) {
  Container.call(this);
  this.id = myId;
  this.className = myClass;
  this.items = myItems;
}

Menu.prototype = Object.create(Container.prototype);
Menu.prototype.constructor = Menu;

Menu.prototype.render = function () {
  this.element = document.createElement('ul');

  for (let i = 0; i < this.items.length; i++) {
    if (this.items[i] instanceof MenuItem) {
      const el = this.items[i].render();
      this.element.appendChild(el);
    }
    if (this.items[i] instanceof SubMenu) {
      const el = this.items[i].render();
      this.element.appendChild(el);
    }
  }
  return this.element;
};

function SubMenu(myClass, myLabel, myItems) {
  Container.call(this);
  this.className = 'sub-menu';
  this.label = myLabel;
  this.items = myItems;
}

SubMenu.prototype = Object.create(Menu.prototype);
SubMenu.prototype.constructor = SubMenu;

SubMenu.prototype.render = function () {
  const ele = document.createElement('li');
  ele.className = this.className;
  ele.innerHTML = `<a href="${this.href}">${this.label}</a>`;

  const elements = document.createElement('ul');

  for (let i = 0; i < this.items.length; i++) {
    if (this.items[i] instanceof MenuItem) {
      const el = this.items[i].render();
      elements.appendChild(el);
    }
  }
  ele.appendChild(elements);
  this.element = ele;
  return this.element;
};

function MenuItem(myHref, myLabel) {
  Container.call(this);
  this.className = 'menu-item';
  this.href = myHref;
  this.label = myLabel;
}

MenuItem.prototype = Object.create(Container.prototype);
MenuItem.prototype.constructor = MenuItem;

MenuItem.prototype.render = function () {
  const el = document.createElement('li');
  el.className = this.className;
  el.innerHTML = `<a href="${this.href}">${this.label}</a>`;
  this.element = el;
  return this.element;
};
