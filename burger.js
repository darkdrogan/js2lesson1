/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
export default class Hamburger {
  constructor(size, stuffing) {
    if (arguments[0] !== Hamburger.SIZE_SMALL && arguments[0] !== Hamburger.SIZE_LARGE) {
      throw new HamburgerException('size');
    }
    if (arguments[1] !== Hamburger.STUFFING_POTATO && arguments[1] !== Hamburger.STUFFING_SALAD) {
      throw new HamburgerException('stuffing');
    }
    this.size = size;
    this.stuffing = stuffing;
    this.topping = [];
  }

  /* Размеры, виды начинок и добавок */
  static SIZE_SMALL() { return { description: 'small burger', cost: 50, calories: 20 }; }

  static SIZE_LARGE() { return { description: 'big burger', cost: 100, calories: 40 }; }

  static STUFFING_CHEESE() { return { description: 'with cheese', cost: 10, calories: 20 }; }

  static STUFFING_SALAD() { return { description: 'with salad', cost: 20, calories: 5 }; }

  static STUFFING_POTATO() { return { description: 'with potato', cost: 15, calories: 10 }; }

  static TOPPING_MAYO() { return { description: 'mayo', cost: 15, calories: 0 }; }

  static TOPPING_SPICE() { return { description: 'spice', cost: 20, calories: 5 }; }

  /**
   * Добавить добавку к гамбургеру. Можно добавить несколько
   * добавок, при условии, что они разные.
   *
   * @param topping     Тип добавки
   * @throws {HamburgerException}  При неправильном использовании
   */
  addTopping(topping) {
    if (!this.topping.includes(topping)) {
      this.topping.push(topping);
    }
  }

  /**
   * Убрать добавку, при условии, что она ранее была
   * добавлена.
   *
   * @param topping   Тип добавки
   * @throws {HamburgerException}  При неправильном использовании
   */
  removeTopping(topping) {
    const toppingIndex = this.topping.indexOf(topping);
    return toppingIndex > -1 ?
      this.topping.splice(toppingIndex, 1) :
      this.topping;

    // TODO: тут не pop: https://stackoverflow.com/questions/5767325/how-do-i-remove-a-particular-element-from-an-array-in-javascript#5767357
    // if (this.topping.includes(topping)) {
    //   return
    // }
    // return this.topping.pop(topping);
  }

  /**
   * Получить список добавок.
   *
   * @return {Array} Массив добавленных добавок, содержит константы
   *                 Hamburger.TOPPING_*
   */
  getToppings() { return this.topping; }

  /**
   * Узнать размер гамбургера
   */
  getSize() { return this.size.description; }

  /**
   * Узнать начинку гамбургера
   */
  getStuffing() { return this.stuffing.description; }

  /**
   * Узнать цену гамбургера
   * @return {Number} Цена в тугриках
   */
  calculatePrice() {
    let price = 0;
    price += this.size.cost;
    price += this.stuffing.cost;
    for (const i in this.topping) {
      price += this.topping[i].cost;
    }
    return price;
  }

  /**
   * Узнать калорийность
   * @return {Number} Калорийность в калориях
   */
  calculateCalories() {
    let calories = 0;
    calories += this.size.calories;
    calories += this.stuffing.calories;
    for (const i in this.topping) {
      calories += this.topping[i].calories;
    }
    return calories;
  }
}
