import Hamburger from './burger';

import HamburgerExecption from './burgerException';

const should = require('should');
describe('Hamburger test', () => {
  let hamburg = {};
  beforeEach(() => {
    hamburg = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_POTATO);
    hamburg.addTopping(Hamburger.TOPPING_MAYO);
    hamburg.addTopping(Hamburger.TOPPING_SPICE);
  });

  it('should create hamburger', () => {
    const hamburg = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_POTATO);
    should(hamburg).instanceOf(Hamburger);
  });

  it('should have expected toppings', () => {
    const expectedToppings = [
      { description: 'mayo', cost: 15, calories: 0 },
      { description: 'spice', cost: 20, calories: 5 }
    ];
    const toppings = hamburg.getToppings();
    should(toppings).eql(expectedToppings);
  });

  it('should have correct price', () => {
    const price = hamburg.calculatePrice();
    should(price).eql(100);
  });

  it('should have correct calories', () => {
    const calories = hamburg.calculateCalories();
    should(calories).eql(35);
  });

  it('should remove topping', () => {
    hamburg.removeTopping(Hamburger.TOPPING_MAYO);
    console.log(hamburg);
  });
});
